from graph import Graph
import numpy as np
import hashlib
import random

class Client():
    def __init__(self):
        self.__G = None
        self.__p_colors = []
        self.__p_keys = []
        self.__ref_color = []

    def generate3ColourGraph(self, valid=True, size=20):
        """
        3-colouring graph generation
        """
        self.__G = Graph(valid, size)
        return self.__G.adjacency_matrix

    def miseEnGageColoriage(self):
        """
        pawn list generation
        """
        self.__p_colors = []
        self.__p_keys = []
        self.__ref_color = self.__G.colorPermutation()
        for i in range(0, self.__G.size):
            self.__p_colors.append(self.__ref_color[self.__G.point_list[i]])
            self.__p_keys.append(np.random.bytes(128))
        return self.__miseEnGageColoriage(self.__p_keys, self.__p_colors)

    def __miseEnGageColoriage(self, p_keys, p_colors):
        """
        used by pawn list generation
        """
        p_gage = []
        for p in range(0, self.__G.size):
            p_gage.append(self.__hash(p_keys[p], p_colors[p]))
        return p_gage

    def getCouplePoint(self, i, j):
        """
        returns a couple of adjacent points
        """
        return ( (i, self.__p_keys[i], self.__p_colors[i]), (j, self.__p_keys[j], self.__p_colors[j]) )


    def __hash(self, key, color):
        """
        generates a hash from a key-color couple
        """
        return hashlib.sha256(key + color.encode('utf-8')).hexdigest()
