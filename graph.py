import math
import random
import numpy as np
import hashlib, binascii
import matplotlib.pyplot as plt
import networkx as nx
from copy import deepcopy

class Graph():
    def __init__(self, valide=True, size=20):
        """
        generates the graph
        """
        self.size = size
        self.colors = ["r", "g", "b"]
        self.color_count = len(self.colors)
        self.point_list = self.generate_colored_points()
        self.adjacency_matrix = self.generate_adjacency_matrix(valide)
        # self.show_graph()
        
    def generate_colored_points(self):
        """
        selects a random color for each node
        """
        return np.random.randint(self.color_count, size=(1, self.size))[0]

    def generate_adjacency_matrix(self, valide):
        """
        generates the adjacency matrix
        """
        adjacency_matrix = np.full((self.size, self.size), 0)
        while len(np.unique(adjacency_matrix, return_counts=True)[0]) == 1:
            adjacency_matrix = np.full((self.size, self.size), 0)
            for p in range(0, self.size):
                for x in range(0, p):
                    if p != x and self.point_list[p] != self.point_list[x]:
                        adjacency_matrix[p][x] = 1
                        adjacency_matrix[x][p] = 1
        if not valide:
            # inject some errors
            rows, cols = np.where(adjacency_matrix == 0)
            zero_occur_idx = list(zip(rows.tolist(), cols.tolist()))
            for i in range(0, math.ceil(len(zero_occur_idx)/3)):
                y, x = zero_occur_idx[i]
                adjacency_matrix[y][x] = 1
                adjacency_matrix[x][y] = 1

        return adjacency_matrix

    def colorPermutation(self):
        """
        mixes the color referential
        """
        tmp = self.colors.copy()
        random.shuffle(tmp)
        return tmp

    def show_graph(self):
        """
        the graph is valid but networkx seems to swap the colors of some nodes 
        (visible by displaying the contents of "colors" and "labels")
        """
        labels = {}
        for i in range(0, self.size):
            labels[i] = i
        colors = []
        for i in self.point_list:
            colors.append(self.colors[i])

        rows, cols = np.where(self.adjacency_matrix == 1)
        edges = zip(rows.tolist(), cols.tolist())
        gr = nx.Graph()
        gr.add_edges_from(edges)
        nx.draw(gr, node_size=500, node_color=colors, labels=labels, with_labels=True)
        plt.show()
