from client import Client
from auditor import Auditor


"""
This scenario describes an "anonymous accreditation" obtained by validating 
that a "client" is able to generate a 3-colouring graph. The validation 
mechanism must control the exactness of the graph generation without 
lifting the "secret" embodied by the colorization of the points.
"""

def scenario(valid, matrix_size):
    # presentation of our protagonists
    client = Client()
    auditor = Auditor()

    # the client generates a graph
    adj_matrix = client.generate3ColourGraph(valid=valid, size=matrix_size)

    # the client provides the adjacency matrix of its graph to the auditor
    auditor.matrixReceiving(adj_matrix)

    # start of experiments aimed at proving that 
    # the client knows how to colour a 3-colouring graph
    test_ok = True
    z = 0
    while test_ok and z <= 400:
        z += 1

        # performs a color permutation then generates 
        # a pawn for each point in the graph
        gages = client.miseEnGageColoriage()

        # transmits the pawns to the auditor
        auditor.pawnReceiving(gages)

        # the auditor wishes to check the colour of two adjacent points
        (i, j) = auditor.couplePointToCheck()

        # the auditor checks the answer
        (p_i, p_j) = client.getCouplePoint(i, j)
        test_ok = auditor.preuveColoriage(p_i, p_j)

        if not test_ok:
            print(f"Authentication failure after {z} tests")
            break

    if test_ok:
        print("The client has been authenticated")


def main():
    matrix_size = 20

    # experience with a valid graph
    print('Case of a valid graph:')
    scenario(True, matrix_size)

    print('\nCase of an invalid graph:')
    # experience with an invalide graph
    scenario(False, matrix_size)


if __name__ == "__main__":
    main()
