import numpy as np
import hashlib
import random

class Auditor():
    def __init__(self):
        self.__size = 0
        self.__adjacency_matrix = None
        self.__adjacent_points = None
        self.__pawns = None

    def matrixReceiving(self, matrix):
        """
        defines the matrix to be used and extracts adjacent points
        """
        # defines the matrix to be used
        self.__adjacency_matrix = matrix
        # saves the size of this matrix
        self.__size = self.__adjacency_matrix.shape[0]
        # extracts adjacent points
        couples = np.where(self.__adjacency_matrix == 1)
        self.__adjacent_points = list(zip(couples[0], couples[1]))

    def pawnReceiving(self, pawn):
        """
        receives the list of pawns and save it
        """
        self.__pawns = pawn

    def couplePointToCheck(self):
        """
        randomly selects a pair of adjacent nodes to be sent back to 
        the client while waiting for an answer containing the key and 
        the color of these nodes to verify that our client knows how 
        to color a 3-colouring graph
        """
        return random.choice(self.__adjacent_points)

    def __hash(self, key, color):
        """
        generates a hash from a key-color couple
        """
        return hashlib.sha256(key + color.encode('utf-8')).hexdigest()

    def preuveColoriage(self, p_i, p_j):
        """
        for the given couple of points, the result conforms to a 
        3-colouring if the (adjacent) nodes are of different colours 
        and the hashes match; the experiment must be reproduced a 
        number "n" high enough to ensure that the client knows how to 
        colour a 3-colouring graph and that it is not just a coincidence
        """
        (i, key_i, color_i) = p_i
        (j, key_j, color_j) = p_j
        if (
            color_i != color_j and 
            self.__hash(key_i, color_i) == self.__pawns[i] and 
            self.__hash(key_j, color_j) == self.__pawns[j]
        ):
            return True
        return False

