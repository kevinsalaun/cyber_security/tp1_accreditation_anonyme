# TP1 - Accréditation Anonyme
Le projet vise à démontrer le fonctionnement d'une accréditation anonyme dont l'obtention nécessite que le demandeur soit capable de réaliser des graphes 3-coloriage. Le mécanisme de validation doit contrôler l'exactitude de la génération du graphique sans lever le "secret" qu'incarne la colorisation des nœuds.

## Comment utiliser le projet
Cloner le projet
```bash
git clone https://gitlab.com/kevinsalaun/cyber_security/tp1_accreditation_anonyme
```

Préparer le virtualenv
```bash
cd tp1_accreditation_anonyme
virtualenv .env -p python3
source .env/bin/activate
pip install -r requirements
```

Lancer le projet
```bash
python main.py
```

## Comment le personnaliser
Dans le fichier "main.py", il y a un scénario avec deux variantes, la première utilise un graphe 3-coloriage valide et l'autre un graphe invalide. Il est possible de jouer sur la variable matrix_size fournie à la fonction "scenario" pour modifier la taille de la matrice à générer.

Le fichier "client.py" contient les caractéristiques propres au client et utilise l'objet "Graph".

Le fichier "auditor.py" émule l'entité responsable de l'authentification du client.

## Analyse du protocole
1. Pourquoi est ce que si l’utilisateur et le vérificateur sont honnêtes et suivent les directives du protocole, un utilisateur possédant la preuve d’un 3-coloriage pourra toujours convaincre le vérificateur (propriété de completeness) ?
   - L'expression "honnêtes" est ici utilisée pour dire que le client et le vérificateur ne mentiront jamais (pas d'altération des données envoyées) et suivront consciencieusement le protocole.
   - Le vérificateur interroge donc le client sur la répartition des couleurs dans ses nœuds afin de vérifier les conditions d'un 3-coloriage: trois couleurs au total et des couleurs différentes entre les nœuds adjacents.
   - Le suivi d'une procédure permet d'obtenir toujours les mêmes résultats, si le client possède un graphe 3-coloriage, il obtiendra son accréditation, autrement, il sera rejeté (sauf si le paramétrage du nombre d'itérations est trop faible).

2. Pourquoi est ce qu’un utilisateur qui ne possède pas de preuve d’un 3-coloriage ne pourra pas réussir à convaincre un vérificateur, sauf avec une probabilité négligeable (propriété de soundness) ?
   - Si le paramétrage du nombre d'itérations est trop faible par rapport au nombre de nœuds contenus dans le graphe, le vérificateur ne va donc vérifier que quelques nœuds et potentiellement se baser sur une coïncidence pour attribuer l'accréditation. 
   - Il y a trois couleurs équitablement réparties dans le graphe, chaque couleur représente donc 1/3 du graphe. La probabilité de piocher, au hasard, successivement deux nœuds de même couleur est de ((N/3)-1)/(N-1), N correspondant au nombre de nœuds dans le graphe. Ayant pour objectif de vérifier que deux nœuds adjacents n'ont pas la même couleur (condition 3-coloriage), nous allons donc retenir la probabilité inverse de celle formulée ci-dessus, nous arrivons donc à l'équation 1-(((N/3)-1)/(N-1)). Nous nous intéressons ensuite à l'évolution de cette probabilité par itération (nombre de fois où l'expérience est reproduite et donnant un résultat positif) représenté dans le tableau suivant (exemple réalisé avec N=20) :


| Nb. itérations (I)  | 1    | 2    | 4    | 6    | 8    | 10   | 20      | 40      | 100      | 400      |
|---------------------|------|------|------|------|------|------|---------|---------|----------|----------|
| Proba. succès (P^I) | 0.70 | 0.49 | 0.24 | 0.12 | 0.06 | 0.03 | 8.39E-4 | 7.04E-7 | 4.15E-16 | 2.98E-62 |

3. Expliquer en quoi ce protocole est à divulgation nulle (propriété de zero-knowledge), c’est à dire qu’il n’apporte aucune autre information au vérificateur que la véracité de l’énoncé ?
   - Le secret est incarné par la colorisation des nœuds. Le vérificateur connaît uniquement la topologie du graphe (fournie par le client), soit l'adjacence des nœuds qui le constitue. 
   - Le client commence par "mettre en gage" ses nœuds en fournissant une liste de hashes au validateur. Un hash est généré à partir d'une clé de 128 bits (aléatoire) et de l'indicateur de couleur du nœud. La particularité de l'indicateur de couleur est qu'il n'engage pas la couleur réelle du nœud, mais seulement la répartition des couleurs dans le graphe. 
   - Le vérificateur demande ensuite au client de lui fournir les clés (128 bits) et les indicateurs de couleur d'un couple de nœuds adjacents afin de calculer leur hash puis de vérifier si celui-ci correspond au hash fournit précédemment (intégrité) et que la couleur des nœuds ne soit pas la même (condition 3-coloriage).
   - Il est important de noter qu'à chaque itération, toute la procédure concernant la génération et la validation des gages est ré-effectuée, ainsi, les clés de 128 bits et les indicateurs de couleur sont de nouveau générés pour préserver le secret en empêchant le validateur de déduire les couleurs du graphe du client. Le diagramme de séquence ci-dessous présente les interactions entre client et validateur:

![image info](./sequential_diagram/sequential_diagram_fr.png "sequential_diagram_fr.png")
