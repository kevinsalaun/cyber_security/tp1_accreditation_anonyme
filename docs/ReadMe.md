# TP1 - Anonymous Accreditation
The project aims at demonstrating the functioning of an anonymous accreditation, the obtaining of which requires the applicant to be able to carry out 3-colouring graphs. The validation mechanism must control the exactness of the graph generation without lifting the "secret" embodied by the colorization of the points.

## How to use
Clone the project
```bash
git clone https://gitlab.com/kevinsalaun/cyber_security/tp1_accreditation_anonyme
```

Preparing the virtualenv
```bash
cd tp1_accreditation_anonyme
virtualenv .env -p python3
source .env/bin/activate
pip install -r requirements
```

Launch the project
```bash
python main.py
```

## How to customize
In the file "main.py" there is a scenario with two variants, the first one uses a valid 3-coloring graph and the other one an invalid one. It is possible to play on the variable matrix_size provided to the "scenario" function to change the size of the matrix to be generated.

The "client.py" file contains the features specific to the client and uses the "Graph" object.

The "auditor.py" file emulates the entity responsible for authenticating the client.

## Protocol analysis
1. Why if the user and the verifier are honest and follow the guidelines of the protocol, a user with proof of 3-colouring will still be able to convince the verifier (completeness property)?
   - The term "honest" is used here to say that the client and the auditor will never lie (no alteration of the data sent) and will follow the protocol conscientiously.
   - The verifier therefore asks the client about the color distribution in its nodes in order to verify the conditions of a 3-coloring: three colors in total and different colors between adjacent nodes.
   - Following a procedure always gives the same results, if the client has a 3-coloring graph, it will get its accreditation, otherwise it will be rejected (unless the number of iterations is too low).

2. Why a user without proof of a 3-colouring cannot convince a verifier, except with negligible probability (soundness property)?
   - If the setting of the number of iterations is too low compared to the number of nodes contained in the graph, the verifier will therefore check only a few nodes and potentially rely on coincidence to award accreditation. 
   - There are three colors equally distributed in the graph, so each color represents 1/3 of the graph. The probability of randomly picking two nodes of the same color in succession is ((N/3)-1)/(N-1), where N corresponds to the number of nodes in the graph. Having for objective to verify that two adjacent nodes do not have the same color (condition 3-coloring), we will thus retain the inverse probability of the one formulated above, we thus arrive at the equation 1-(((N/3)-1)/(N-1)). We are then interested in the evolution of this probability by iteration (number of times the experiment is reproduced and giving a positive result) represented in the following table (example carried out with N=20):


| Iteration number (I) | 1    | 2    | 4    | 6    | 8    | 10   | 20      | 40      | 100      | 400      |
|----------------------|------|------|------|------|------|------|---------|---------|----------|----------|
| Success prob. (P^I)  | 0.70 | 0.49 | 0.24 | 0.12 | 0.06 | 0.03 | 8.39E-4 | 7.04E-7 | 4.15E-16 | 2.98E-62 |

3. Explain how this protocol is zero-knowledge, in other words, how it provides no information to the auditor other than the truthfulness of the statement?
   - The secret is embodied by the colouring of the nodes. The verifier knows only the topology of the graph (provided by the client), i.e. the adjacency of the nodes that make up the graph. 
   - The client begins by "pawning" its nodes by providing a list of hashes to the auditor. A hash is generated from a 128-bit (random) key and the node's color indicator. The special feature of the color indicator is that it does not engage the actual color of the node, but only the distribution of colors in the graph. 
   - The verifier then asks the client to provide the keys (128 bits) and color indicators of a pair of adjacent nodes in order to calculate their hash and then to check whether it matches the previously provided hash (integrity) and whether the color of the nodes is not the same (3-color condition).
   - It is important to note that at each iteration, the whole procedure concerning the generation and validation of the pledges is re-performed, so the 128-bit keys and color indicators are generated again to preserve the secret by preventing the auditor from deducting the colors composing the client's graph. The sequence diagram below shows the interactions between client and auditor:

![image info](./sequential_diagram/sequential_diagram_fr.png "sequential_diagram_en.png")
